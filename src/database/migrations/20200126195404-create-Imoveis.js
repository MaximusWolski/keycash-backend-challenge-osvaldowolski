
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Imoveis',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    codigo: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    endereco: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    complemento: {
      allowNull:  true,
      type: Sequelize.STRING,
   },
    bairro: {
      allowNull: false,
      type: Sequelize.STRING,
   },
    cidade: {
      allowNull: false,
      type: Sequelize.STRING,
   },
    uf: {
      allowNull: false,
      type: Sequelize.STRING,
   },
   cep: {
     allowNull: false,
     type: Sequelize.STRING,
   },
   vagas: {
     allowNull: true,
     type: Sequelize.INTEGER,
     defaultValue: '0',
   },
   quartos: {
     allowNull: false,
     type: Sequelize.INTEGER,
     defaultValue: '1',
   },
   banheiros: {
     allowNull: false,
     type: Sequelize.INTEGER,
     defaultValue: '1',
   },
   extras: {
     allowNull: true,
     type: Sequelize.TEXT,
   },
   observacoes: {
     allowNull: true,
     type: Sequelize.TEXT,
   },
   valor_aluguel: {
     allowNull: true,
     type: Sequelize.FLOAT(8,2),
   },
   valor_venda: {
     allowNull: true,
     type: Sequelize.FLOAT(8,2),
   },
   createdAt: {
     allowNull: false,
     type: Sequelize.DATE,
   },
   updatedAT: {
     allowNull: false,
     type: Sequelize.DATE,
   }
  }),

  down: (queryInterface, Sequelize) => queryInterface.dropTable('imoveis') 
};
