'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('Imoveis', 
    [
      {
        codigo: '100',
        endereco: 'Rua Tobias de Aguiar, 41',
        complemento: '',
        bairro: 'Centro',
        cidade: 'Guarulhos',
        uf: 'SP',
        cep: '03152-020',
        vagas: '32',
        quartos: '0',
        banheiros: '8',
        extras: 'Galpão para uso empresarial - somente venda',
        observacoes: 'Espaço para manobra de caminhões',
        valor_aluguel: '0.00',
        valor_venda: '285000',
        createdAT: '2020-01-26 20:31:45',
        updatedAt: '2020-01-26 20:31:47',
      },
      {
        codigo: '200',
        endereco: 'Rua Tabaupuã 1541',
        complemento: '5. andar',
        bairro: 'Itaim Bibi',
        cidade: 'São Paulo',
        uf: 'SP',
        cep: '02152-020',
        vagas: '2',
        quartos: '0',
        banheiros: '4',
        extras: 'Sala Empresarial - somente aluguel',
        observacoes: 'Mezanino pronto',
        valor_aluguel: '3250',
        valor_venda: '0.00',
        createdAT: '2020-01-26 20:31:48',
        updatedAt: '2020-01-26 20:31:49',
      },
      {
        codigo: '300',
        endereco: 'Rua Gil Amora,154',
        complemento: 'Térreo',
        bairro: 'Parada Inglesa',
        cidade: 'São Paulo',
        uf: 'SP',
        cep: '02107-020',
        vagas: '1',
        quartos: '2',
        banheiros: '1',
        extras: 'Lavanderia no subsolo',
        observacoes: 'Garagem para carro pequeno',
        valor_aluguel: '1750',
        valor_venda: '0.00',
        createdAT: '2020-01-26 20:31:50',
        updatedAt: '2020-01-26 20:31:51',

      }      
    ], {}),

  down: (queryInterface) => queryInterface.bulkDelete('Users', null, {}),
};



