const { Imoveis } = require('../models/');

class ImovelController {

//listando todos os registros do banco de dados
  async index(req, res) {
    try {
      const imoveis = await Imoveis.findAll();

      return res.json(imoveis);
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  }

  //listando somente o imovel selecionado
  async show(req, res) {
    try {
      const imoveis = await Imoveis.findByPk(req.params.id);

      return res.json(imoveis);
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  }

  //criando um novo imovel
  async store(req, res) {
    try {
      const imoveis = await Imoveis.create({         
      codigo: req.body.codigo,
      endereco: req.body.endereco,
      complemento: req.body.complemento,
      bairro: req.body.bairro,
      cidade: req.body.cidade,
      uf: req.body.uf,
      cep: req.body.cep,
      vagas: req.body.vagas,
      quartos: req.body.quartos,
      banheiros: req.body.banheiros,
      extras: req.body.extras,
      observacoes: req.body.observacoes,
      valor_aluguel: req.body.valor_aluguel,
      valor_venda: req.body.valor_venda,
      createdAT: "2020-01-26 20:31:45",
      updatedAt: "2020-01-26 20:31:47", });
      console.log(imoveis);
      return res.json(imoveis);
    } catch (err) {
      return res.status(400).json({ error: err.message });
      
    }
    
  }

  async update(req, res) {
    try {
      const imovel = await Imoveis.findByPk(req.params.id);

      await Imovel.update(req.body);

      return res.json({ imovel });
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  }

async search (req, res) {
  try {
    const imoveis = await sequelize.query("SELECT * FROM `Imoveis` where quartos = req.params.qry");
    return res.json(imoveis);
  }catch (err){
    return res.status(400).json({ error: err.message })
  }
}


  async destroy(req, res) {
    try {
      const imovel = await Imoveis.findByPk(req.params.id);

      await imovel.destroy();

      return res.json();
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  }
}

module.exports = new ImovelController();