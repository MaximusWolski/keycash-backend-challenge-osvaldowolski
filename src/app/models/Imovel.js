

module.exports = (sequelize, DataTypes) => {
    const Imoveis = sequelize.define('Imoveis', {
      codigo: {
          type: DataTypes.STRING,
      },
      endereco: {
          type: DataTypes.STRING,
      },
      complemento: {
          type: DataTypes.STRING,
      },
      bairro: {
          type: DataTypes.STRING,
      },
      cidade: {
          type: DataTypes.STRING,
      },
      uf: {
          type: DataTypes.STRING,
      },
      cep: {
          type: DataTypes.STRING,
      },
      vagas: {
          type: DataTypes.INTEGER,   
      },
      quartos: {
          type: DataTypes.INTEGER,
      },
      banheiros: {
          type: DataTypes.INTEGER,
      },
      extras: {
          type: DataTypes.TEXT,
      },
      observacoes: {
          type: DataTypes.TEXT,
      },
      valor_aluguel: {
          type: DataTypes.FLOAT,
      },
      valor_venda: {
          type: DataTypes.FLOAT,
      },
      createdAt: {
          type: DataTypes.DATE,
      },
      updatedAt: {
          type: DataTypes.DATE,
      }
    });
  
    return Imoveis;
  };