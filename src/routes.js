const { Router } = require('express');
const ImovelController = require('./app/controllers/ImovelController');

const routes = Router();

routes.get('/', ImovelController.index);

routes.get('/imovel/:id', ImovelController.show);

routes.post('/pesquisa/string', ImovelController.search);

routes.post('/novo', ImovelController.store);

routes.put('/imovel/:id', ImovelController.update);

routes.delete('/imovel/:id', ImovelController.destroy);

module.exports = routes;