# README #

Keycash Backend Code Challenge

### versão 1.0.0. do teste ####


### Como rodar a aplicação local? ###

 baixe o git
 baixe as dependências
 npm i express --save
 npm i sequelize --save
 npm i mysql2
 npm i sequeluze-cli

### Conecte o banco dados no arquivo ###
 edite o conteudo do arquivo src/config/database.js para os dados de seu banco

### Crie o banco de dados com o comando ###
 npx sequelize db:create

### Crie as tabelas com o comando ###
 npx sequelize db:migrate

### Rode os seeders com o comando ###
 npx sequelize db:seed;all

### Rodando a aplicação ####
 acesse o diretorio onde a aplicação foi baixada e rode o comando abaixo:
 
 node src/index.js

### Rotas ###
Rotas do sistema:  
  GET   / :lista todos 
  GET	/imovel/:id : exibe registro unico 
  POST 	/novo : grava novo 
  PUT   /imovel/:id: deleta registro
  POST  /pesquisa : efetua pesquisa (Não implementada)
  
  
  Modelagem do Banco de dados:
  AS características da tabela do banco de dados estão disponíveis em:
  
  src/database/migrations/20200126195404-create-Imoveis.js 
  